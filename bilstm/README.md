# Convolutional Recurrent Neural Network + CTCLoss 

## Dependence

- CentOS7
- Python3.6.5
- torch==1.2.0
- torchvision==0.4.0
- Tesla P40 - Nvidia

## Inferencing

- Download a pretrained model from the drive [https://drive.google.com/file/d/1Ml1D98nyqanyywD-kTGqY-vivrxgZ4LA/view?usp=drive_link ](https://drive.google.com/drive/folders/1ypeX-vog2mheo0tEBhfbPyS6LDDN3q6w?usp=drive_link)
- run bilstm/demo as follows
- 
```sh
python demo.py -m path/to/model.pth -i data/demo.jpg
```

## Training

- Download a pretrained model from the drive [https://drive.google.com/file/d/1Ml1D98nyqanyywD-kTGqY-vivrxgZ4LA/view?usp=drive_link ](https://drive.google.com/drive/folders/1ypeX-vog2mheo0tEBhfbPyS6LDDN3q6w?usp=drive_link)
  - Make sure you take the one that currently needs to be used for inferencing / training
- Download your preferred dataset from the drive (e.g. Hindi) [https://drive.google.com/drive/folders/1PpM-dPIM9QXeNZCzNgLUHIVV5MSN6GRv?usp=drive_link](https://drive.google.com/drive/folders/1PpM-dPIM9QXeNZCzNgLUHIVV5MSN6GRv?usp=drive_link)
  - Alternatively you can download the datasets yourself, and run the create_IIIT and create_ROY notebooks (make sure to change the folder references) from test_notebooks, to create the proper files. (train, test, val)
- For each of the (train, test, val) folders, run the following command to create the lmdb files:
- 
```sh
  python tool/create_dataset.py --out lmdb/data/output/path --folder path/to/folder
  ```



- If pretraining: go to params.py => pretrained and insert the file location
- Else: go to params.py => pretrained and put ''

- Run `train.py` by

```sh
python train.py --trainroot path/to/train/dataset --valroot path/to/val/dataset
```


### Change alphabets

- Change alphabets

  Please put all the alphabets appeared in your labels to `alphabets.py` , or the program will throw error during training process.



## Reference

[meijieru/crnn.pytorch](<https://github.com/meijieru/crnn.pytorch>)

[Sierkinhane/crnn_chinese_characters_rec](<https://github.com/Sierkinhane/crnn_chinese_characters_rec>)

